package haoru.cisco.com.a;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AApplication {

	public static void main(String[] args) {
        System.out.println("[AApplication.java:10] " + "hello");
		SpringApplication.run(AApplication.class, args);
	}
}
